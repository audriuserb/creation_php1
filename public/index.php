<?php
// define application directories
define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);
define('MODEL', ROOT . 'app' . DIRECTORY_SEPARATOR. 'model' . DIRECTORY_SEPARATOR);
define('CONTROLLER', ROOT . 'app' . DIRECTORY_SEPARATOR. 'controller' . DIRECTORY_SEPARATOR);
define('VIEW', ROOT . 'app' . DIRECTORY_SEPARATOR. 'view' . DIRECTORY_SEPARATOR);
define('CORE', ROOT . 'app' . DIRECTORY_SEPARATOR. 'core' . DIRECTORY_SEPARATOR);
define('UPLOADS', ROOT . 'public' . DIRECTORY_SEPARATOR. 'uploads' . DIRECTORY_SEPARATOR);
// create directory definitions array
$modules = [ROOT,APP,MODEL,CONTROLLER,VIEW,CORE,UPLOADS];
//define URL
define('URL_PUBLIC_FOLDER', 'public');
define('URL_PROTOCOL', '//');
define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);
//register directories for autoloading
set_include_path(get_include_path().PATH_SEPARATOR.implode(PATH_SEPARATOR,$modules));
//start autoloading
spl_autoload_register('spl_autoload',false);
//invoke Application class
new Application;



