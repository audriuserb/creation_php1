<?php
/**
 * DataTablesController class
 * 
 * renders dataTables page
 * 
 * @author      Audrius Serbentavičius <serbentavicius.audrius@gmail.com>
 * @version     1.0                 
 * @since       1.0         
 */
class DataTablesController extends Controller
{
    /**
     * index method
     *
     * Firstly model class with modelName parameter is initiated, then view class with
     * viewName and data as parameters is initiated. In this case data is array got 
     * from model class function getData. Finally view class render method is initiated
     * which renders the page.
     * 
     * @access public
     */
    public function index()
    {
        $this->model('data');
        $this->view('dataTables\index',$this->model->getData());
        $this->view->render();
    }
}
?>