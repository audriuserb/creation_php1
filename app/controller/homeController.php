<?php
/**
 * HomeController class
 * 
 * renders home page
 * 
 * @author      Audrius Serbentavičius <serbentavicius.audrius@gmail.com>
 * @version     1.0                 
 * @since       1.0         
 */
class HomeController extends Controller
{
    /**
     * renders home page
     * 
     * First the view class is initiated with viewName as parameter, then
     * view class render method is initiated, which renders the page.
     * 
     * @access public
     */
    public function index()
    {
        $this->view('home/index');
        $this->view->render();
    }
}
?>