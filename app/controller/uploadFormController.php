<?php
/**
 * UploadFormController class
 * 
 * renders uploadForm page and handles form data
 * 
 * @author      Audrius Serbentavičius <serbentavicius.audrius@gmail.com>
 * @version     1.0                 
 * @since       1.0         
 */  
class UploadFormController extends Controller
{
  /**
   * index method
   *
   * First the view class is initiated with viewName as parameter, then
   * view class render method is initiated, which renders the page.
   * 
   * @param array $data form submission messages
   * @access public
   */
  public function index($data = [])
  {
    $this->view('uploadForm\index',$data);
    $this->view->render();
  }

  /**
   * uploadFiles method
   *
   * Handles form data. Ensures that file does not exist in uploads directory,
   * allows only csv, xml and json format files. Uploads files to uploads directory
   * then redirects back to uploadFormController with messages array as parameters.
   * 
   * @access public
   */
  public function uploadFiles()
  {
    $messages = [];
    //counts number of files in the request
    $total = isset($_FILES["fileToUpload"]["name"]) ? count($_FILES["fileToUpload"]["name"]) : 0;
    for( $i=0 ; $i < $total ; $i++ )
    {
      $file_name = basename($_FILES["fileToUpload"]["name"][$i]);
      $target_file = UPLOADS.basename($_FILES["fileToUpload"]["name"][$i]);
      $uploadOk = 1;
      $fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
      // if file exists in uploads directory, do not upload the file
      if (file_exists($target_file)) {
        array_push($messages,"Sorry, file ".$file_name." already exists.");
        $uploadOk = 0;
      }
      // if file format is not csv, xml or json, do not upload the file
      if($fileType != "csv" && $fileType != "xml" && $fileType != "json") {
        array_push($messages,"Sorry, file ".$file_name." cannot be uploaded. Only csv, XML & JSON files are allowed.");
        $uploadOk = 0;
      }
      // if there are errors with current file, push error message to $messages array, else try to upload file
      if ($uploadOk == 0) {
        array_push($messages,"Sorry, file ".$file_name." was not uploaded.");
      } else {
        //try to upload file
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$i], $target_file)) {
          //file upload succeeded
          array_push($messages,"The file ". basename( $_FILES["fileToUpload"]["name"][$i]). " has been uploaded.");
        } else {
          //file upload failed
          array_push($messages,"Sorry, there was an error uploading file ".$file_name." .");
        }
      }
    }
    //redirect to uploadForm controller index method with $messages as parameters
    header("Location: /uploadForm/index/".http_build_query ($messages));
    exit;
  }
}
?>
