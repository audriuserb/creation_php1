<?php
/**
 * Controller class
 * 
 * Initiates view and model
 * 
 * @author      Audrius Serbentavičius <serbentavicius.audrius@gmail.com>
 * @version     1.0                 
 * @since       1.0         
 */
class Controller
{
    /**
     * @var mixed $view a variable for storing view class
     * @access protected
     *
     * @var mixed $model a variable for storing model class
     * @access protected
     */
    protected $view;
    protected $model;

    /**
     * view method
     *
     * initiates view class
     * 
     * @param mixed $viewName specifies view file name
     * @param array $data specifies parameters sent to view class
     * @access public
     */
    public function view($viewName,$data = []){
        $this->view = new View($viewName,$data);
    }

    /**
     * model method
     *
     * initiates model class
     * 
     * @param mixed $viewName specifies model file name
     * @param array $data specifies parameters sent to model class
     * @access public
     */
    public function model($modelName,$data = [])
    {
        $this->model = new $modelName($data);
    }
}
?>