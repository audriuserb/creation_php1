<?php
/**
 * Application class
 * 
 * Routes application url requests according to MVC architecture
 * 
 * @author      Audrius Serbentavičius <serbentavicius.audrius@gmail.com>
 * @version     1.0                 
 * @since       1.0         
 */
class Application{
    /** 
     * @var string $controller a variable for storing controller name extracted from URL 
     * @access protected
     *
     * @var string $action a variable for storing action name extracted from URL 
     * @access protected
     *
     * @var array $controller an array for storing parameters extracted from URL 
     * @access protected
     */
    protected $controller = 'homeController';
    protected $action = 'index';
    protected $parameters = [];

    /**
     * __construct method
     *
     * Initiates prepareURL, checks if controller file exists, checks if method in specified
     * controller exists, then initiates described controller's method with specified parameters
     * 
     * @access public
     */
    public function __construct(){
        $this->prepareURL();
        if(file_exists(CONTROLLER.$this->controller.'.php')){
            $this->controller = new $this->controller;
            if(method_exists($this->controller,$this->action)){
                call_user_func_array([$this->controller,$this->action],$this->parameters);
            }
        }
    }

    /**
     * prepareURL method
     *
     * Trims request url, then explodes it to array. If first array element exists it is assigned 
     * as controller, if not default value of 'homeController' is assigned. Then if second array 
     * element exists it is assigned as action, if not default value of 'index' is assigned. Then
     * url array's first two values are unset and remaining elements are assigned to parameters array.
     * 
     * @access protected
     */
    protected function prepareURL(){
        $request = trim($_SERVER['REQUEST_URI'],'/');
        if(!empty($request)){
            $url = explode('/',$request);
            $this->controller = isset($url[0]) ? $url[0].'Controller':'homeController';
            $this->action = isset($url[1]) ? $url[1]:'index';
            unset($url[0],$url[1]);
            $this->parameters = !empty($url) ? array_values($url) : [];
        }
    }
}
?>