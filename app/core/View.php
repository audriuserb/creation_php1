<?php
/**
 * View class
 * 
 * Renders view
 * 
 * @author      Audrius Serbentavičius <serbentavicius.audrius@gmail.com>
 * @version     1.0                 
 * @since       1.0         
 */
class View
{
    /**
     * @var mixed $view_file a variable for storing view file name
     * @access protected
     *
     * @var mixed $view_data a variable for storing data
     * @access protected
     */
    protected $view_file;
    protected $view_data;

    /**
     * __construct method
     *
     * @param mixed $view_file a variable for storing view file name
     * @param mixed $view_data a variable for storing data
     */
    public function __construct($view_file,$view_data)
    {
        $this->view_file = $view_file;
        $this->view_data = $view_data;
    }

    /**
     * render method
     *
     * Checks if view, header and footer files exists, if so include them.
     * 
     * @access public
     */
    public function render()
    {
        if(file_exists(VIEW.$this->view_file.'.phtml') 
            && file_exists(VIEW.'_templates/header.phtml')
            && file_exists(VIEW.'_templates/footer.phtml')){
            include VIEW.'_templates/header.phtml';
            include VIEW.$this->view_file.'.phtml';
            include VIEW.'_templates/footer.phtml';
        }
    }

    /**
     * getAction method
     *
     * Extracts action name from viewFile variable
     * 
     * @return string an action name
     * @access public
     */
    public function getAction(){
        return (explode('\\',$this->view_file)[0]);
    }
}
?>