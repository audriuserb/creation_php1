<?php
/**
 * Data class
 * 
 * Reads data from files 
 * 
 * @author      Audrius Serbentavičius <serbentavicius.audrius@gmail.com>
 * @version     1.0                 
 * @since       1.0         
 */
class Data
{
    /**
     * @var array $data_file a variable for storing data file name
     * @access protected
     *
     * @var array $view_data an array for storing data extracted from files
     * @access protected
     */
    protected $data_file = [];
    protected $data = array();

    /**
     * loadData method
     *
     * Gets file names from uploads directory. Then parses the files according to their's extentions.
     * 
     * @access private
     */
    private function loadData()
    {
        foreach(new DirectoryIterator(UPLOADS) as $file) {
            if($file->isFile()){
                array_push($this->data_file,$file->getFilename());
            }
        }
        foreach($this->data_file as $key => $value){
            if(file_exists(UPLOADS.$value))
            {
                //if file extention is csv
                if(strtolower(pathinfo(UPLOADS.$value,PATHINFO_EXTENSION)) == "csv"){
                    //maps csv file data to array
                    $rows   = array_map(function($file){return str_getcsv($file, ",","'");}, file(UPLOADS.$value));
                    //shifts column names from $rows array to $header array
                    $header = array_shift($rows);
                    foreach($rows as $row) {
                        //combines $rows and $header arrays to associative array
                        $this->data[$value][] = array_combine($header, $row);
                    }
                } else if(strtolower(pathinfo(UPLOADS.$value,PATHINFO_EXTENSION)) == "xml"){
                    //if file extention is xml, load xml file, encode it to json and then decode it to associative array
                    $this->data[$value] = json_decode(json_encode(simplexml_load_file(UPLOADS.$value,"SimpleXMLElement")->xpath('item')),true);
                } else if (strtolower(pathinfo(UPLOADS.$value,PATHINFO_EXTENSION)) == "json"){
                    // if file extention is json, decode it to associative array
                    $this->data[$value] = json_decode(file_get_contents(UPLOADS.$value),true);
                }   
            }
        }
    }

    /**
     * getData method
     *
     * Initiate data parsing method loadData, then return extracted data
     * 
     * @return array data extracted from files
     * @access public
     */
    public function getData()
    {
        $this->loadData();
        return $this->data;
    }
}
?>