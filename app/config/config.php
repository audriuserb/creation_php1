<?php
/**
 * URL_PUBLIC_FOLDER:
 * Aplankas, kurį gali pasiekti įprastas vartotojas, kitų aplankų pvz "app" jis pasiekti negalės.
 *
 * URL_PROTOCOL:
 * Nurodomas puslapio protokolas, kadangi SSL nebus naudojamas, adresas gausis "http://" - "//"
 *
 * URL_DOMAIN:
 * Nurodomas puslapio domenas, gaunamas iš serverio šiuo atveju naudojant xampp bus localhost
 *
 * URL_SUB_FOLDER:
 * Nurodomas sub-folderis, šiuo atveju jo nenaudojame, kadangi xampp konfiguracijoje nurodėme kelią į "public" katalogą,
 * todėl ši "define" funkcija tiesiog po domeno uždės "/"
 *
 * URL:
 * Iš aukščiau nurodytų URL parametrų sugeneruojamas URL adresas
 */

define('URL_PUBLIC_FOLDER', 'public');
define('URL_PROTOCOL', '//');
define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);